
## Task 1 - Creating Menus

In this project we'll be using Laravel 5.7. Use as reference for the prerequisites and installation process the [documentation](https://laravel.com/docs/5.7/installation).
Here is a short [article](https://laravel.com/docs/5.7/installation) you can also read.

Clone the repo for the project: git clone https://Tulev@bitbucket.org/Tulev/menus-demo.git

After generating an application key, you'll need to edit your .env file and make sure you are connected to a database.
Change these with your credentials and you should be good to go.

DB_DATABASE=
DB_USERNAME=
DB_PASSWORD=

Next run the command from your project's root: 'php artisan migrate' and then: 'php artisan serve --port=8080'.
 Head to http://127.0.0.1:8080 and you should see the 'Welcome' page.

#### Step 1
Create migration, model and a factory for the Menus.
- fields: 
id, name, uri, hidden, timestamp ... (target_blank boolean field for extra credit :) )

What's our goal?
- when you run: 'php artisan migrate:refresh --seed'  

#### Step 2

In the project's [route/web.php](#) file you will find 

## Task 2 - Implement Sub-Menus.

## Task 3 - Implement translatable menus.

## Task 4 - Implement 'nested set' to the menus' model.
